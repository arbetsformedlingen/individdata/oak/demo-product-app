# Demo Product App:

To run demo product app:

1. You need to run a community server. 

2. You need to run oak control panel.

3. You need to run Demo Consumer App (
    run this before start server node setup-pods, node gen-sink-token)
    To start the server: node sink-notification-client.js

4. Then you can run Demo Product App (
    run this also before start server node gen-source-token
)
    To start the server: node source-notification-client.js